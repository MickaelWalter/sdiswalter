package server;

public interface QoSListener {

	/**
	 * Notifies the QoSListener
	 * @param timestamp the timestamp of the occured event
	 */
	void notify(String timestamp);

}
