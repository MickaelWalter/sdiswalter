package server;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;

public class QoSManager implements QoSListener {
	/**
	 * Indicates in seconds the low bound to decide to remove a worker
	 */
	private final static int MIN_WAIT = 1;
	/**
	 * Indicates in seconds the high bound to decide to add a worker
	 */
	private final static int MAX_WAIT = 4;
	/**
	 * Indicates the minimum number of workers that are active
	 */
	private final static int MIN_WORKERS = 1;
	/**
	 * Indicates the maximum number of workers that are active
	 */
	private final static int MAX_WORKERS = 1000;
	
	private ArrayList<Worker> workers;
	private long lastMean;
	private long lastDelay;
	
	public QoSManager() {
		lastMean = 0;
		lastDelay = 0;
		workers = new ArrayList<Worker>();
		for(int i = 0; i < MIN_WORKERS && i < MAX_WORKERS; i++)
			addWorker();
	}

	/**
	 * Adds a new worker, runs it and binds it to the manager
	 */
	private void addWorker() {
		Worker newWorker = new Worker();
		newWorker.addListener(this);
		
		newWorker.start();
		workers.add(newWorker);
	}
	
	/**
	 * Remove a worker bound to the manager
	 */
	private void removeWorker() {
		Worker toRemove = workers.remove(workers.size()-1);
		toRemove.stopWorker();
		toRemove.interrupt();
	}

	@Override
	public void notify(String timestamp) {
		Timestamp time = Timestamp.valueOf(timestamp);
		Date date= new java.util.Date();
		Timestamp now = new Timestamp(date.getTime());
		System.out.println("Current mean : " + (now.getTime()/1000-time.getTime()/1000+lastMean/1000)/2);
		if((now.getTime()-time.getTime()+lastMean)/2 >= MAX_WAIT*1000 && workers.size() < MAX_WORKERS)
			addWorker();
		else if((now.getTime()-time.getTime()+lastMean)/2 <= MIN_WAIT*1000 && workers.size() > MIN_WORKERS)
			removeWorker();
		lastMean = (int) ((lastDelay + now.getTime()-time.getTime())/2);
		lastDelay = now.getTime()-time.getTime();
	}
}
