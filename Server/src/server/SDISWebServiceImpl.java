package server;

import javax.jws.WebService;

@WebService
public class SDISWebServiceImpl implements SDISWebService {

	private TaskManager manager;
	
	public SDISWebServiceImpl() {
		manager = new TaskManager();
	}
	
	@Override
	public String newTask(String length) {
		try {
			manager.addTask(length);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "Task of "+length+" seconds added to the queue";
	}

}
