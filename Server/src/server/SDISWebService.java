package server;

import javax.jws.WebMethod;  
import javax.jws.WebService;  
  
@WebService  
public interface SDISWebService {  
	 @WebMethod
	 public String newTask(String length); 
}
