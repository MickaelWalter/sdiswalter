package server;

import java.io.IOException;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

public class ChannelSingleton {
	public static final String SERVER="ec2-54-73-194-42.eu-west-1.compute.amazonaws.com";
	public static final int PORT=5672;
	public static final String TASK_QUEUE_NAME = "task_queue_walter";
	private static Connection connection;
	private static Channel channel;
	
	public static void init() {
		if(connection != null && channel != null)
			return;
		
		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost(SERVER);
		factory.setPort(PORT);
		
		try {
			connection = factory.newConnection();
			channel = connection.createChannel();
			channel.queueDeclare(TASK_QUEUE_NAME, true, false, false, null);
		} catch (IOException e) {
			e.printStackTrace();
			System.err.println("Unable to connect to the RabbitMQ server");
		}
	}
	
	public static Connection getConnection() {
		if(connection == null) 
			init();
		return connection;
	}
	
	public static Channel getChannel() {
		if(channel == null)
			init();
		return channel;
	}
	
	public static void closeChannel() {
		if(connection == null || !connection.isOpen())
			return;
		
		try {
			connection.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		connection = null;
		channel = null;
	}
	
}
