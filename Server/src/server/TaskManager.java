package server;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.Date;

import com.rabbitmq.client.MessageProperties;

public class TaskManager {
	private QoSManager qosManager;
	
	/**
	 * Inits the task manager, opening a message queue
	 */
	public TaskManager() {
		// Queue init
		ChannelSingleton.init();
		//QoS init
		qosManager = new QoSManager();
	}
	
	/**
	 * Adds a new task in the queue for the workers
	 * @param length, the length of the task in seconds
	 * @throws Exception if something happens with the queue
	 */
	public void addTask(String length) {
		//The message corresponds to the length of the task, this is for simulation purpose
		Date date= new java.util.Date();
		Timestamp time = new Timestamp(date.getTime());
		try {
			ChannelSingleton.getChannel().basicPublish( "", ChannelSingleton.TASK_QUEUE_NAME,
			MessageProperties.PERSISTENT_TEXT_PLAIN,
			(time + "," + length).getBytes());
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println(" ["+date+"] Sent a task of " + length + "s");
	}
	
	/**
	 * Closes the connection. Should be called when exiting.
	 */
	public void closeConnection() {
		ChannelSingleton.closeChannel();
	}
}