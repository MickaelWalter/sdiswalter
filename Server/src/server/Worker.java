package server;

import java.io.IOException;
import java.util.ArrayList;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.ConsumerCancelledException;
import com.rabbitmq.client.QueueingConsumer;
import com.rabbitmq.client.ShutdownSignalException;

public class Worker extends Thread {
	private Channel channel;
	private QueueingConsumer consumer;
	private ArrayList<QoSListener> listeners;
	
	public Worker() {
		listeners = new ArrayList<QoSListener>();
		channel = ChannelSingleton.getChannel();
		consumer = new QueueingConsumer(channel);
	}
	
	@Override
	public void run() {
		System.out.println(" [*] Worker launched");
		
		try {
			channel.basicConsume(ChannelSingleton.TASK_QUEUE_NAME, false, consumer);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		while (true) {
			QueueingConsumer.Delivery delivery;
			try {
				delivery = consumer.nextDelivery();
				String message = new String(delivery.getBody());
				System.out.println(" [x] Received a task of " + message + "s");
				String[] tasks = message.split(",");
				notifyListeners(tasks[0]);
				int seconds = Integer.parseInt(tasks[1]);
				if(seconds > 0) {
					Thread.sleep(seconds*1000);
				}
				channel.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
			} catch (ShutdownSignalException | ConsumerCancelledException
					| InterruptedException | IOException e) {
			}
			
			System.out.println(" [x] Done");
		}
	}
	
	/**
	 * Adds a listener to operate QoS
	 * @param listener
	 */
	public void addListener(QoSListener listener) {
		listeners.add(listener);
	}
	
	/**
	 * Removes a listener to operate QoS
	 * @param listener
	 */
	public void removeListener(QoSListener listener) {
		listeners.remove(listener);
	}
	
	/**
	 * Notifies all the listeners
	 * @param timestamp the timestamp of the event
	 */
	private void notifyListeners(String timestamp) {
		for(QoSListener listener : listeners) {
			listener.notify(timestamp);
		}
	}
	
	/**
	 * Stops the worker
	 */
	public void stopWorker() {
		try {
			channel.basicCancel(consumer.getConsumerTag());
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("[x] Worker stopped");
	}
}