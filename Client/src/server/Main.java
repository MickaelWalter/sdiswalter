package server;

import java.util.Scanner;



public class Main {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
        SDISWebServiceImplService newTaskService = new SDISWebServiceImplService();  
        SDISWebServiceImpl newTask = newTaskService.getSDISWebServiceImplPort();  
        
        while(true) {
	        System.out.println("Enter task execution time and press enter (q to quit)");
	        String line = sc.nextLine();
	        if(line.equals("q")) {
	        	System.out.println("Bye");
	        	sc.close();
	        	return;
	        }
	        try {
	        	Integer.parseInt(line);
	        	System.out.println(newTask.newTask(line));
	        } catch (NumberFormatException e) {
	        	System.out.println("Bye");
	        	sc.close();
	        	return;
	        }
        }
	}

}
