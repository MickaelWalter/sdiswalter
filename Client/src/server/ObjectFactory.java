
package server;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the server package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _NewTaskResponse_QNAME = new QName("http://server/", "newTaskResponse");
    private final static QName _NewTask_QNAME = new QName("http://server/", "newTask");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: server
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link NewTask }
     * 
     */
    public NewTask createNewTask() {
        return new NewTask();
    }

    /**
     * Create an instance of {@link NewTaskResponse }
     * 
     */
    public NewTaskResponse createNewTaskResponse() {
        return new NewTaskResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NewTaskResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://server/", name = "newTaskResponse")
    public JAXBElement<NewTaskResponse> createNewTaskResponse(NewTaskResponse value) {
        return new JAXBElement<NewTaskResponse>(_NewTaskResponse_QNAME, NewTaskResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NewTask }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://server/", name = "newTask")
    public JAXBElement<NewTask> createNewTask(NewTask value) {
        return new JAXBElement<NewTask>(_NewTask_QNAME, NewTask.class, null, value);
    }

}
