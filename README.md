To run this program, you must have an RabbitMQ server.
You can use the one defined by default in the code.
For your own server please check https://www.rabbitmq.com/.